const path = require('path')

module.exports = {
	entry: './src/app.ts',
	output: {
		filename: './game.js',
		path: __dirname
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: 'ts-loader'}
		]
	}
};