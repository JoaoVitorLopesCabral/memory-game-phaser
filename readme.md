How to setup

Run ```npm install```

Run ```npm run build``` to build the game or ```npm run watch``` to keep running the compiler while editing the code

Start a http server on the root folder (where index.html is located)
if not http server is installed, you can use the following

```npm install -g http-server```

then

```http-server . -c-1 -o```

Game tiles: Extracted from Stardew Valley wiki - Art rights belong to ConcernedApe 
https://stardewvalleywiki.com/Stardew_Valley_Wiki - https://stardewvalley.net/

Trophy art: "Trophy Cups Pixel Pack" by Vsio NeithR - https://neithr.itch.io/trophy-cups-pixel-packs