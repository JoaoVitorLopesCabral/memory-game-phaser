/**
 * Checks if the given value is within range
 * 
 * @param rangeStart Range start (min value)
 * @param rangeEnd Range end (max value)
 * @param value Value to check
 */
function InRange( rangeStart: number, rangeEnd: number, value: number ) : boolean {
	return rangeStart <= value && value <= rangeEnd
}

export {
	InRange
}