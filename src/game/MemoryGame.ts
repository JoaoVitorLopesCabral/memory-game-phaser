/// <reference path="../../defs/phaser.comments.d.ts" />

import { Grid } from './interfaces/Grid'
import { GameTile } from './interfaces/GameTile'
import shuffle from './../utils/Shuffle'
import { InRange } from './../utils/Range'
import { Margin } from './interfaces/Margin';

/**
 * Game grid (will store 16 tiles)
 */
var GameGrid: Grid
/**
 * Global game instance
 */
var globalGameInstance: Phaser.Game

/**
 * Get the margins based on the viewport
 * @param GameInstance Phaser.Game instance
 */
function getGridMargins( GameInstance: Phaser.Game ) : Margin {
	let MarginLeft = ( GameInstance.scale.width - ( 64 * 4 ) ) / 2 

	return {
		Left: MarginLeft,
		Top: 32
	}
}

/**
 * Scramble tiles present in GameGrid.Tiles
 */
function scrambleTiles() {
	if ( GameGrid.Tiles == null ) {
		throw new Error('No tiles available! Cannot run scramble')
	}

	let GameMargins = getGridMargins( globalGameInstance )

	let Shuffled = shuffle( GameGrid.Tiles )
	GameGrid.Tiles = Shuffled.map( (Tile, TileIndex) => {
		let TileXFactor: number = 0
		let TileYFactor: number = 0

		// Adjust tile position
		if ( InRange( 0, 3, TileIndex ) ) {
			TileXFactor = TileIndex
		}
		else if ( InRange( 4, 7, TileIndex ) ) {
			TileXFactor = TileIndex - 4
			TileYFactor = 1
		}
		else if ( InRange( 8, 11, TileIndex ) ) {
			TileXFactor = TileIndex - 8
			TileYFactor = 2
		}
		else {
			TileXFactor = TileIndex - 12
			TileYFactor = 3
		}

		Tile.Sprite.x = ( TileXFactor * 64 ) + GameMargins.Left
		Tile.Sprite.y = ( TileYFactor * 64 ) + GameMargins.Top

		return Tile
	})	
}
/**
 * Check if the flipped up tiles match
 */
function matchTiles() : boolean {
	let FlippedTiles = GameGrid.Tiles.filter( t => t.IsTileFlippedUp && !t.IsMatchedWithAnotherTile )

	let KeyToMatch = ''
	let Match = false

	FlippedTiles.forEach( (Tile, TileIndex) => {
		if ( KeyToMatch == '' ) {
			KeyToMatch = Tile.ImageKey
		}
		else if ( KeyToMatch == Tile.ImageKey ) {
			Match = true
		}
	})

	if ( Match ) {
		// Update tiles
		FlippedTiles.forEach( (Tile, TileIndex) => {
			Tile.IsMatchedWithAnotherTile = true

			updateTile( Tile )
		})
	}

	// Increment number of tries
	if ( FlippedTiles.length == 2 ) {
		GameGrid.NumOfTries++
	}

	return Match
}

/**
 * Updates de tile data in GameGrid.Tiles
 * 
 * @param Tile Tile to be updated
 */
function updateTile( Tile: GameTile ) {
	let TileIndex = GameGrid.Tiles.indexOf( GameGrid.Tiles.find( t => t.Sprite == Tile.Sprite ) )

	GameGrid.Tiles[TileIndex] = Tile
}

/**
 * Flips a tile
 * 
 * @param Tile Tile to flip
 */
function flipTile( Tile: GameTile ) {
	Tile.Sprite.play( 'flipping', 30 ).onComplete.add(() => {
		Tile.Sprite.play( Tile.IsTileFlippedUp ? 'visible' : 'hidden' )
	})
}

/**
 * Executed whenever the users clicks on a Tile
 * Updates the Tile list with the current tile state
 * 
 * @param Tile Tile that received the input event
 */
function tilePostInput( Tile: GameTile ) {
	// Update tile
	updateTile( Tile )

	// Check if there is a match
	let FoundMatch = matchTiles()
	let FlippedTiles = GameGrid.Tiles.filter( t => t.IsTileFlippedUp && !t.IsMatchedWithAnotherTile )
	
	if ( FlippedTiles.length == 2 && !FoundMatch ) {
		setTimeout( () => {
			// Flip down tiles
			FlippedTiles.forEach( (Tile, TileIndex) => {
				Tile.IsTileFlippedUp = false
				updateTile( Tile )

				// Play animation
				flipTile( Tile )
			})
		}, 750 )
	}
}

/**
 * Check if all pairs have been found
 */
function isGameOver() : boolean {
	return GameGrid.Tiles.filter( t => !t.IsMatchedWithAnotherTile ).length == 0
}

/**
 * Creates a new tile and add it to the game
 * 
 * @param GameInstance Phase.Game instance
 * @param TileSheet Spritesheet name
 * @param X X position
 * @param Y Y position
 */
function tileFactory( GameInstance: Phaser.Game, TileSheet: string, X: number, Y: number ) {
	let Sprite = GameInstance.add.sprite(X, Y, TileSheet)
	Sprite.animations.add('hidden', [0])
	Sprite.animations.add('flipping', [1, 2, 3, 4, 5, 6])
	Sprite.animations.add('visible', [7])

	Sprite.inputEnabled = true

	let IsUp = false

	Sprite.events.onInputDown.add(() => {
		// Ignore input if the tile is already matched
		if ( GameGrid.Tiles.find( t => t.Sprite == Sprite ).IsMatchedWithAnotherTile ) {
			return
		}
		// Check if there are 2 tiles alread flipped
		if ( GameGrid.Tiles.filter( t => t.IsTileFlippedUp && !t.IsMatchedWithAnotherTile ).length == 2 ) {
			// The tile hasn't been updated yet, just ignore the input
			return
		}

		// Get tile being modified
		let Tile = GameGrid.Tiles.find( t => t.Sprite == Sprite )
		if ( !Tile.IsTileFlippedUp ) {
			// Flip tile
			flipTile( Tile )

			Tile.IsTileFlippedUp = !Tile.IsTileFlippedUp
		}

		tilePostInput( Tile )
	})

	GameGrid.Tiles.push({
		Sprite: Sprite,
		IsTileFlippedUp: IsUp,
		ImageKey: TileSheet,
		IsMatchedWithAnotherTile: false
	})
}

/**
 * Reset tiles and rescramble
 */
function restartGame() {
	scrambleTiles()

	// Flip all up
	GameGrid.Tiles.forEach( (Tile, TileIndex) => {
		Tile.IsTileFlippedUp = true
		Tile.IsMatchedWithAnotherTile = false

		flipTile( Tile )
	})

	setTimeout( () => {
		GameGrid.Tiles.forEach( (Tile, TileIndex) => {
			Tile.IsTileFlippedUp = false
			Tile.IsMatchedWithAnotherTile = false
	
			flipTile( Tile )
		})
	}, 3000 )	

	GameGrid.NumOfTries = 0
}

export class MemoryGame {
	private game: Phaser.Game
	private attemptsText: Phaser.Text
	private trophy: Phaser.Sprite

	protected create() {
		// Initialize game grid
		GameGrid = {
			Tiles: [],
			NumOfTries: 0
		}

		tileFactory( this.game, 'beer', 0, 0 )
		tileFactory( this.game, 'beer', 0, 0 )
		tileFactory( this.game, 'chicken', 0, 0 )
		tileFactory( this.game, 'chicken', 0, 0 )
		tileFactory( this.game, 'duck', 0, 0 )
		tileFactory( this.game, 'duck', 0, 0 )
		tileFactory( this.game, 'grape', 0, 0 )
		tileFactory( this.game, 'grape', 0, 0 )
		tileFactory( this.game, 'prismatic', 0, 0 )
		tileFactory( this.game, 'prismatic', 0, 0 )
		tileFactory( this.game, 'strawberry', 0, 0 )
		tileFactory( this.game, 'strawberry', 0, 0 )
		tileFactory( this.game, 'sunflower', 0, 0 )
		tileFactory( this.game, 'sunflower', 0, 0 )
		tileFactory( this.game, 'wheat', 0, 0 )
		tileFactory( this.game, 'wheat', 0, 0 )

		// Call restart to scramble and display tiles
		restartGame()
		
		this.trophy = this.game.add.sprite( 0, ( 64 * 4 ) + ( 2 * getGridMargins( this.game ).Top ), 'trophy' )
		this.trophy.position.x = ( this.game.scale.width - this.trophy.width ) / 2
		this.trophy.visible = false

		this.trophy.animations.add( 'bronze', [0] )
		this.trophy.animations.add( 'silver', [1] )
		this.trophy.animations.add( 'gold', [2] )
		this.trophy.animations.add( 'platinum', [3] )
		
		this.attemptsText = this.game.add.text( 0, this.trophy.bottom + 16, '', { font: '16px Arial', fill: '#FFFFFF', align: 'center'} )

		let RestartButton = this.game.add.button( 100, this.trophy.bottom + ( 2 * getGridMargins( this.game ).Top ) , 'buttonRestart' )
		RestartButton.position.x = ( this.game.scale.width - RestartButton.width ) / 2		

		RestartButton.events.onInputDown.add(() => {
			this.trophy.visible = false
			this.attemptsText.visible = false
			restartGame()
		})
	}
	/**
	 * Load game assets
	 */
	protected preload() {
		this.game.load.spritesheet( 'beer', 'assets/tile-beer.png', 64, 64 )
		this.game.load.spritesheet( 'chicken', 'assets/tile-chicken.png', 64, 64 )
		this.game.load.spritesheet( 'duck', 'assets/tile-duck.png', 64, 64 )
		this.game.load.spritesheet( 'grape', 'assets/tile-grape.png', 64, 64 )
		this.game.load.spritesheet( 'prismatic', 'assets/tile-prismaticshard.png', 64, 64 )
		this.game.load.spritesheet( 'strawberry', 'assets/tile-strawberry.png', 64, 64 )
		this.game.load.spritesheet( 'sunflower', 'assets/tile-sunflower.png', 64, 64 )
		this.game.load.spritesheet( 'wheat', 'assets/tile-wheat.png', 64, 64 )

		this.game.load.image( 'buttonRestart', 'assets/button-restart.png' )

		this.game.load.spritesheet( 'trophy', 'assets/trophy.png', 32, 32 )
	}

	protected update() {
		if ( isGameOver() ) {
			if ( GameGrid.NumOfTries == 8 ) {
				this.trophy.play( 'platinum' )
			}
			else if ( InRange( 9, 12, GameGrid.NumOfTries ) ) {
				this.trophy.play( 'gold' )
			}
			else if ( InRange( 13, 20, GameGrid.NumOfTries ) ) {
				this.trophy.play( 'silver' )
			}
			else {
				this.trophy.play( 'bronze' )
			}

			this.trophy.visible = true
			this.attemptsText.text = `Pairs flipped: ${GameGrid.NumOfTries}`
			this.attemptsText.position.x = ( this.game.scale.width - this.attemptsText.width ) / 2
			this.attemptsText.visible = true
		}
	}

	constructor() {
		this.game = new Phaser.Game('100', '100', Phaser.AUTO, '', {
			create: this.create,
			preload: this.preload,
			update: this.update
		})

		globalGameInstance = this.game
	}
}