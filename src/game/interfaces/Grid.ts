import { GameTile } from "./GameTile";

export interface Grid {
	/**
	 * Array containing all tiles
	 */
	Tiles: Array<GameTile>
	/**
	 * Number of tries (pairs flipped) before finishing
	 */
	NumOfTries: number
}