export interface GameTile {
	/**
	 * Phaser.Sprite instance (rendered to screen)
	 */
	Sprite: Phaser.Sprite
	/**
	 * Gets wheter this tile is flipped over
	 */
	IsTileFlippedUp: boolean
	/**
	 * Image representing this tile
	 */
	ImageKey: string
	/**
	 * Gets wheter the tile was matched to another tile
	 */
	IsMatchedWithAnotherTile: boolean
}